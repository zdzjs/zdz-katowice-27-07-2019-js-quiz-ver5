/*
	https://developer.mozilla.org/pl/docs/Web/API/Window/localStorage
*/
function setStorageValue(name,value) {
	localStorage.setItem(name,value);
}

function getStorageValue(name) {
	let a = localStorage.getItem(name)
	return a===null ? undefined : a;
}

function getStorageValuesAll() {
	var wyjscie={};
	Object.keys(localStorage).forEach(k => {wyjscie[k]=localStorage.getItem(k);});
	return wyjscie;
}

function deleteStorageValue(name) {
	localStorage.removeItem(name);
}

function deleteStorageValuesAll() {
	localStorage.clear();
}
/*
	Działa identycznie do sessionStorage lecz zapisane tutaj dane są "wieczne", tj.
	usuwane dopiero wraz z pamięcią podręczną przeglądarki. Przykłady analogiczne do
	sessionStorage (zmiana nazw samych funkcji).
*/